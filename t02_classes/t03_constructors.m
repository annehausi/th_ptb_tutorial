%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_toolbox');

%% get a human and let him say hello
my_human = example01.AdvancedHuman('Adam');
my_human.say_name();